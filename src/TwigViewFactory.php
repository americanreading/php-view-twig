<?php

namespace AmericanReading\View;

use OutOfBoundsException;
use Twig\Environment;

class TwigViewFactory implements ViewFactory
{
    private Environment $twig;
    private array $templateRoots;
    private array $defaultContext;

    /**
     * TwigViewFactory constructor.
     * @param Environment $twig
     * @param string|array $templateRoots
     * @param array $defaultContext
     */
    public function __construct(
        Environment $twig,
        $templateRoots,
        array $defaultContext
    ) {
        $this->twig = $twig;

        if(is_string($templateRoots)) {
            $this->templateRoots = [$templateRoots];
        } elseif(is_array($templateRoots)) {
            $this->templateRoots = $templateRoots;
        } else {
            $this->templateRoots = [];
        }

        $this->defaultContext = $defaultContext;
    }

    public function getView(string $template): View
    {
        foreach ($this->templateRoots as $root) {
            $templatePath = $root . '/' . $template;
            if (file_exists($templatePath)) {
                return new TwigView(
                    $this->twig,
                    $this->defaultContext,
                    $template
                );
            }
        }
        throw new OutOfBoundsException("No template found for $template");
    }
}
