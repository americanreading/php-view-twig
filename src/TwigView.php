<?php

namespace AmericanReading\View;

use Twig\Environment;

class TwigView implements View
{
    /** @var Environment */
    private $twig;
    /** @var array */
    private $defaultContext;
    /** @var string */
    private $template;

    /**
     * @param Environment $twig
     * @param array $defaultContext
     * @param string $template
     */
    public function __construct(
        Environment $twig,
        array $defaultContext,
        string $template
    ) {
        $this->twig = $twig;
        $this->defaultContext = $defaultContext;
        $this->template = $template;
    }

    public function render(array $context): string
    {
        $mergedContext = $this->getMergedContext($context);
        $template = $this->twig->load($this->template);
        return $template->render($mergedContext);
    }

    private function getMergedContext(array $context): array
    {
        return array_replace_recursive($this->defaultContext, $context);
    }
}
