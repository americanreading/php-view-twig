<?php

namespace AmericanReading\ThemeMap\Test\View;

use AmericanReading\View\TwigViewFactory;
use OutOfBoundsException;
use PHPUnit\Framework\TestCase;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

/**
 * Integration tests for the View and ViewFactory using a working Twig renderer.
 */
class ViewTest extends TestCase
{
    private $factory;

    private function initStringRootFactory():void
    {
        $templateRoot = realpath(__DIR__ . '/views');
        $loader = new FilesystemLoader($templateRoot);
        $conf = [
            'debug' => true,
            'cache' => false,
            'autoescape' => false
        ];
        $twig = new Environment($loader, $conf);
        $defaultContext = [];
        $this->factory = new TwigViewFactory($twig, $templateRoot, $defaultContext);
    }

    private function initArrayRootFactory():void
    {
        $templateRoot = [
            realpath(__DIR__ . '/views'),
            realpath(__DIR__ . '/moreViews'),
        ];
        $loader = new FilesystemLoader($templateRoot);
        $conf = [
            'debug' => true,
            'cache' => false,
            'autoescape' => false
        ];
        $twig = new Environment($loader, $conf);
        $defaultContext = [];
        $this->factory = new TwigViewFactory($twig, $templateRoot, $defaultContext);
    }

    public function setUp(): void
    {
        $this->initStringRootFactory();
    }

    public function testViewFactoryProvidesFunctioningViewForStringRoot()
    {
        $view = $this->factory->getView('sample.html.twig');
        $rendered = $view->render([
            'name' => 'World'
        ]);
        $this->assertEquals('<h1>Hello, World!</h1>', trim($rendered));
    }

    public function testViewFactoryThrowsExceptionForInvalidViewForStringRoot()
    {
        $this->expectException(OutOfBoundsException::class);
        $this->factory->getView('invalid.html.twig');
    }

    public function testViewFactoryProvidesFunctioningViewForArrayRoot()
    {
        $this->initArrayRootFactory();

        // first root
        $view = $this->factory->getView('sample.html.twig');
        $rendered = $view->render([
            'name' => 'World'
        ]);
        $this->assertEquals('<h1>Hello, World!</h1>', trim($rendered));

        // second root
        $view = $this->factory->getView('sample2.html.twig');
        $rendered = $view->render([
            'name' => 'World'
        ]);
        $this->assertEquals('<h1>Hello, World!</h1>', trim($rendered));
    }

    public function testViewFactoryThrowsExceptionForInvalidViewForArrayRoot()
    {
        $this->initArrayRootFactory();
        $this->expectException(OutOfBoundsException::class);
        $this->factory->getView('invalid.html.twig');
    }
}
