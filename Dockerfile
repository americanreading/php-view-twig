FROM php:7.4

RUN apt-get update \
  && apt-get -y install \
    git \
    unzip \
    wget \
    zip \
  && apt-get clean

# Download Composer
RUN curl -sS https://getcomposer.org/installer | php -- \
  --filename=composer --install-dir=/usr/local/bin

# Install XDebug
RUN pecl install xdebug \
  && docker-php-ext-enable xdebug

# Add symlink for phpunit for easier running
RUN ln -s /usr/local/src/arc/vendor/bin/phpunit /usr/local/bin/phpunit

# Create a user
RUN useradd -ms /bin/bash arc
USER arc

WORKDIR /usr/local/src/arc
