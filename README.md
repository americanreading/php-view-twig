# Twig Views

Twig-backed implementation of `View` and `ViewFactory`.

To use, include with Composer:

```json
{
  "require": {
    "americanreading/view-twig": "^1",
    "twig/twig": "^2"
  }
}
```

Configure a `Twig_Environment` and pass this to the `TwigView` constructor along with the root path to the view templates.

```php
$templateRoot = '/path/to/twig/template/files';
$loader = new \Twig_Loader_Filesystem($templateRoot);
$conf = [
    'debug' => true,
    'cache' => false,
    'autoescape' => false
];
$twig = new \Twig_Environment($loader, $conf);
// Provide a context array to use as a default for all views.
$defaultContext = [
  'cat' => 'Molly'
];
// Create the factory.
$factory = new TwigViewFactory($twig, $templateRoot, $defaultContext);
```

Use the factory to provide `View` instances for a given template file:

```php
$view = $factory->getView('my-template.twig.html');
```

Render the view to a string, merging in a context array.

```php
$html = $view->render(['dog' => 'Bear']);
```

